<?php

/**
 * @file
 * Enables modules and site configuration for the Blog technology profile.
 */

/**
 * Implements hook_install_tasks_alter().
 */
function blog_technology_install_tasks_alter(&$tasks, $install_state) {
  module_load_include('profile', 'blog');
  _blog_install_tasks_alter($tasks, $install_state);
}
