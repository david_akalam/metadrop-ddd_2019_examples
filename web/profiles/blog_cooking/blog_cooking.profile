<?php

/**
 * @file
 * Enables modules and site configuration for the Blog cooking profile.
 */

/**
 * Implements hook_install_tasks_alter().
 */
function blog_cooking_install_tasks_alter(&$tasks, $install_state) {
  module_load_include('profile', 'blog');
  _blog_install_tasks_alter($tasks, $install_state);
}
