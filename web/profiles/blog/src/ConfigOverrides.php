<?php

namespace Drupal\blog;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Class ConfigOverrides.
 *
 * @package Drupal\blog
 */
class ConfigOverrides implements ConfigFactoryOverrideInterface {

  const CONFIG_SPLIT_PREFIX = 'config_split.config_split';

  /**
   * List of installed modules.
   *
   * @var array
   */
  protected $moduleList;

  /**
   * The installation profile.
   *
   * @var string
   */
  protected $profile;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * SolvayConfigOverrides constructor.
   *
   * @param array $module_list
   *   The module list.
   * @param string $profile
   *   The installation profile.
   */
  public function __construct(array $module_list, $profile) {
    $this->moduleList = $module_list;
    $this->profile = $profile;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    $overrides = [];

    // Only calculate overrides when $names are config_splits. We use blog_cooking
    // config_split as representative member of config split family.
    if (in_array(self::CONFIG_SPLIT_PREFIX . '.blog_cooking', $names)) {
      $overrides += $this->confSplitOverrides($names);
    }

    return $overrides;
  }

  /**
   * Configuration split overrides.
   *
   * @param array $names
   *   A list of configuration names that are being loaded.
   *
   * @return array
   *   An array keyed by configuration name of override data. Override data
   *   contains a nested array structure of overrides.
   */
  protected function confSplitOverrides(array $names) {
    $overrides = [];

    // If there is a config_split named as a installed module, activate it.
    foreach ($this->moduleList as $module) {
      if ($module['type'] == 'module') {
        $split_name = self::CONFIG_SPLIT_PREFIX . '.' . basename($module['pathname'], '.info.yml');
        if (in_array($split_name, $names, TRUE)) {
          $overrides[$split_name]['status'] = TRUE;
        }
      }
    }

    // Activate the config_split for the installation profile.
    $split_name = self::CONFIG_SPLIT_PREFIX . '.' . $this->profile;
    if (in_array($split_name, $names, TRUE)) {
      $overrides[$split_name]['status'] = TRUE;
    }

    // Activate the config_split for the current site, based on multisite folder.
    $site_path = \Drupal::service('site.path');
    $site_path = explode('/', $site_path);
    $site_name = $site_path[1];
    $split_name = self::CONFIG_SPLIT_PREFIX . '.site_' . $site_name;
    if (in_array($split_name, $names, TRUE)) {
      $overrides[$split_name]['status'] = TRUE;
    }

    // Activate the config_split for the current environment, based on URL.
    $host = \Drupal::request()->getHost();
    if (strpos($host, '.docker.localhost') !== FALSE) {
      $env = 'local';
    }
    else if (strpos($host, 'dev.') === 0) {
      $env = 'dev';
    }
    else if (strpos($host, 'www.') === 0) {
      $env = 'prod';
    }
    $split_name = self::CONFIG_SPLIT_PREFIX . '.env_' . $env;
    if (in_array($split_name, $names, TRUE)) {
      $overrides[$split_name]['status'] = TRUE;
    }

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return self::class;
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    $cacheable_metadata = new CacheableMetadata();
    if (strpos($name, self::CONFIG_SPLIT_PREFIX) !== FALSE) {
      $cacheable_metadata->addCacheTags(['config:core.extension']);
    }

    return $cacheable_metadata;
  }

}
