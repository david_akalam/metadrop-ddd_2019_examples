<?php

namespace Drupal\blog\Plugin\ConfigFilter;

use Drupal\config_filter\Plugin\ConfigFilterBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Excludes a set of extensions from exporting to core.extension.yml.
 *
 * @ConfigFilter(
 *   id = "blog_exclude_profile",
 *   label = "Allows to exclude the profile from core.extension.yml.",
 *   storages = {"config.storage.sync"},
 *   weight = 10
 * )
 */
class ExcludeProfile extends ConfigFilterBase implements ContainerFactoryPluginInterface {

  /**
   * The installation profile.
   *
   * @var string
   */
  protected $profile;

  /**
   * ExcludeExtensions constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param string $profile
   *   The installation profile.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, string $profile) {
    $this->profile = $profile;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('install_profile')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function filterWrite($name, array $data) {
    if ($name == 'core.extension' && isset($data['profile'])) {
      unset($data['profile']);
    }
    return parent::filterWrite($name, $data);
  }

  /**
   * {@inheritdoc}
   */
  public function filterRead($name, $data) {
    if ($name == 'core.extension') {
      $data['profile'] = $this->profile;
    }
    return parent::filterRead($name, $data);
  }

  /**
   * {@inheritdoc}
   */
  public function filterReadMultiple(array $names, array $data) {
    if (in_array('core.extension', $names)) {
      $data['core.extension'] = $this->filterRead('core.extension', $data['core.extension']);
    }
    return $data;
  }

}
