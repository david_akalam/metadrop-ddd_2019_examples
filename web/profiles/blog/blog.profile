<?php

/**
 * @file
 * Enables modules and site configuration for the blog profile.
 */

/**
 * Implements hook_install_tasks_alter().
 */
function blog_install_tasks_alter(&$tasks, $install_state) {
  _blog_install_tasks_alter($tasks, $install_state);
}

/**
 * Add custom tasks for profile installing.
 *
 * @param array $tasks
 *   The tasks to alter.
 * @param array $install_state
 *   The current install state.
 */
function _blog_install_tasks_alter(array &$tasks, array $install_state) {
  // Use the Drupal 8.6 way of importing from existing config.
  if (!$install_state['parameters']['existing_config']) {
    // @codingStandardsIgnoreLine
    global $install_state;
    $install_state['parameters']['existing_config'] = TRUE;
  }

  // The installation profile has been removed from core.extension.yml
  // but this config is required for a complete installation.
  if ($install_state['active_task'] == 'install_config_import_batch') {
    $profile = $install_state['parameters']['profile'];
    \Drupal::configFactory()->getEditable('core.extension')
      ->set('profile', $profile)
      ->save();
  }

  if (isset($tasks['install_config_import_batch']) && !isset($tasks['install_profile_modules'])) {
    // This tasks is actually doing the opposite of what we are aiming for.
    // It tries to revert to a partial config state.
    unset($tasks['install_config_revert_install_changes']);

    $key = array_search('install_config_import_batch', array_keys($tasks), TRUE);
    $config_tasks = [
      // This one is working around config dependencies likely due to wrong
      // split.
      'install_profile_modules' => [
        'display_name' => t('Install site'),
        'type' => 'batch',
      ],

      // This one is actually importing config again so that splits are taken
      // into account. See install_config_import_batch().
      'blog_install_config_import_batch' => $tasks['install_config_import_batch'],
    ];
    $tasks = array_slice($tasks, 0, $key + 1, TRUE) +
      $config_tasks +
      array_slice($tasks, $key, NULL, TRUE);
  }
}


/**
 * See install_config_import_batch().
 */
function blog_install_config_import_batch() {
  return install_config_import_batch();
}
